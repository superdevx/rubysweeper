require './lib/minesweeper'

describe Minesweeper::Grid do
  it "should produce a grid of the given dimensions" do
    actual = Minesweeper::Grid.new(5, 5, 0)

    expect(actual.dimensions).to eq(25)
  end

  describe "#unseal" do
    it "should produce cells that stay open when they are unsealed" do
      actual = Minesweeper::Grid.new(5, 5, 4, 23)
      actual.unseal(1, 2)

      expect(actual.cell(1, 2).open?).to eq(true)
    end

    it "should unseal (recursively open) cells and their neighbours, that are not mines" do
      actual = Minesweeper::Grid.new(5, 5, 4, 23)
      actual.unseal(4, 3)

      expect(actual.open_cells.count).to eq(6)
    end

    it "should not open neighbouring cells which are mines" do
      actual = Minesweeper::Grid.new(5, 5, 4, 23)
      actual.unseal(2, 3)

      expect(actual.cell(2, 4).mine?).to eq(true)
      expect(actual.cell(2, 4).open?).to eq(false)
    end
  end

  describe "#count_adjacent_mines" do
    it "should return the number of mines adjacent to a cell" do
      actual = Minesweeper::Grid.new(5, 5, 4, 23)

      expect { actual.count_adjacent_mines(1, 2).to eq(4) }
    end
  end

  describe "#cell" do
    it "should raise an 'OutOfRange' error if x or y is out of the boundaries of the grid" do
      actual = Minesweeper::Grid.new(5, 5, 0)

      expect { actual.cell(5, 4) }.to raise_error(Minesweeper::Errors::OutOfRange)
      expect { actual.cell(-1, 1) }.to raise_error(Minesweeper::Errors::OutOfRange)
    end
  end

  describe "#neighbours" do
    it "should return 8 neighbours for a centred cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(2, 2).count

      expect(actual).to eq(8)
    end

    it "should return 5 neighbours for an edge cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(0, 3).count

      expect(actual).to eq(5)
    end

    it "should return 3 neighbours for the top-left cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(0, 0).count

      expect(actual).to eq(3)
    end

    it "should return 3 neighbours for the bottom-left cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(0, 4).count

      expect(actual).to eq(3)
    end

    it "should return 3 neighbours for the top-right cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(4, 0).count

      expect(actual).to eq(3)
    end

    it "should return 3 neighbours for the bottom-right cell" do
      grid = Minesweeper::Grid.new(5, 5, 0)
      actual = grid.neighbours(4, 4).count

      expect(actual).to eq(3)
    end
  end
end

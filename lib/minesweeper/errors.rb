module Minesweeper
  module Errors
    class OutOfRange < StandardError
      def initialize(coord)
        super("Coordinate (#{coord[0]}, #{coord[1]}) out of range")
      end
    end
    class CellOpen < StandardError
      def initialize(coord)
        super("Cell (#{coord[0]}, #{coord[1]}) is already open")
      end
    end
    class InvalidStatus < StandardError
      def initialize()
        super("Status is invalid")
      end
    end
  end
end

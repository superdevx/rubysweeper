module Minesweeper
  class Grid
    def initialize(x, y, num_mines, seed = nil)
      srand seed unless seed.nil?

      @grid = []
      non_mines = x * y - num_mines

      non_mines.times do ||
        @grid << Cell.new(false)
      end

      num_mines.times do ||
        @grid << Cell.new(true)
      end

      @grid = @grid.shuffle

      @rows = x
      @columns = y
    end

    attr_reader :rows
    attr_reader :columns

    def count_adjacent_mines(i, j)
      neighbours(i, j).count { |cell| cell[1].mine? }
    end

    def out_of_range?(i, j)
      raise(Minesweeper::Errors::OutOfRange, [i, j]) if underflow?(i, j) || overflow?(i, j)
    end

    def soft_out_of_range?(i, j)
      underflow?(i, j) || overflow?(i, j)
    end

    def cell(i, j)
      out_of_range?(i, j)

      @grid[i*@columns + j]
    end

    def cleared?
      @grid.any? { |cell| !cell.mine? && !cell.open? }
    end

    def state
      @grid.map { |cell| cell.value }
    end

    def neighbours(i, j)
      [
        [i - 1, j + 1],
        [i - 1, j ],
        [i - 1, j - 1],
        [i, j + 1],
        [i, j - 1],
        [i + 1, j + 1],
        [i + 1, j],
        [i + 1, j - 1],
      ]
        .reject { |coord| soft_out_of_range?(*coord) }
        .map { |coord| [coord, cell(*coord)] }
    end

    def unseal(i, j)
      current_cell = cell(i, j)

      return :game_lost if current_cell.mine?

      return :game_ongoing if current_cell.open?

      current_cell.open

      adjacent = neighbours(i, j)
      current_cell.value = adjacent.count { |cell| cell[1].mine? }

      return :game_ongoing unless current_cell.value.zero?

      adjacent
          .reject { |neighbour| neighbour[1].mine? || neighbour[1].open? }
          .each { |neighbour| unseal(*neighbour[0]) }

      cleared? ? :game_won : :game_ongoing
    end

    def open_cells
      @grid.select { |n| n.open? }
    end

    def dimensions
      @rows * @columns
    end

    private
    def overflow?(i, j)
      i > ( @rows - 1 ) || j > ( @columns - 1 )
    end

    def underflow?(i, j)
      i < 0 || j < 0
    end
  end
end

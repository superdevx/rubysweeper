module Minesweeper
  class Cell
    def initialize(is_mine)
      @is_mine = is_mine
      @is_open = false
      @value
    end

    attr_accessor :value

    def mine?
      @is_mine
    end

    def open
      @is_open = true
    end

    def open?
      @is_open
    end
  end
end

require './lib/minesweeper'
game = Minesweeper::Game.new(5, 5, 4)

loop do
  x = gets.chomp.to_i - 1
  y = gets.chomp.to_i - 1
  puts game.unseal(x, y)
  break if game.status == :game_lost
end

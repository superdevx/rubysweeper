require './lib/minesweeper'

describe Minesweeper::Game do
  describe "#unseal" do
    it "should lose the game if used on a mine" do
      actual = Minesweeper::Game.new(5, 5, 4, 23)
      actual.unseal(2, 4)

      expect(actual.status).to eq(:game_lost)
    end
  end
end

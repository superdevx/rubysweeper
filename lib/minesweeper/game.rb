module Minesweeper
  class Game
    def initialize(x, y, num_mines, seed = nil)
      @grid = Grid.new(x, y, num_mines, seed)
      @status = :game_ongoing
    end

    attr_reader :status

    def unseal(i, j)
      return "This game has ended with status #{@status}." if @status != :game_ongoing

      @status = @grid.unseal(i, j)

      case @status
      when :game_lost
        "You lost!"
      when :game_won
        "You won!\n\n" + display
      else
        display
      end
    end

    def cell_to_string(cell)
      cell.nil? ? " . " : " #{cell} "
    end

    def display
      @grid.state.map{ |cell| cell_to_string(cell) }.each_slice(@grid.columns).to_a
      .reverse
      .map { |cell| cell.join("") }.join("\n")
    end

    def inspect
      "\n" + display
    end
  end
end

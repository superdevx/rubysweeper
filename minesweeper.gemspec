Gem::Specification.new do |s|
  s.name        = 'minesweeper'
  s.version     = '0.0.0'
  s.date        = '2018-07-20'
  s.summary     = "Minesweeper!"
  s.description = "A simple minesweeper gem"
  s.authors     = ["Ben Ahmady"]
  s.files       = ["lib/minesweeper.rb"]
  s.license     = 'MIT'
end

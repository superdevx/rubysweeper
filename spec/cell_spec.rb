require './lib/minesweeper'

describe Minesweeper::Cell do
  it "should be a mine" do
    actual = Minesweeper::Cell.new(is_mine = true).mine?
    expect(actual).to be(true)
  end

  it "should not be a mine" do
    actual = Minesweeper::Cell.new(is_mine = false).mine?
    expect(actual).to be(false)
  end

  it "should not be open, initially" do
    actual = Minesweeper::Cell.new(is_mine = true).open?
    expect(actual).to be(false)
  end
end
